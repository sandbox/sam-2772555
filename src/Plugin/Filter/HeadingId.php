<?php

namespace Drupal\heading_id_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * The filter to turn tokens inserted into the WYSIWYG into videos.
 *
 * @Filter(
 *   title = @Translation("Heading ID"),
 *   id = "heading_id_filter",
 *   description = @Translation("Adds an ID attribute to all headings."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class HeadingId extends FilterBase {

  /**
   * Get the options for headings to process.
   *
   * @return array
   *   An array of heading options.
   */
  protected function getHeadings() {
    return array_map(function ($number) {
      return 'h' . $number;
    }, range(1, 5));
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $html_dom = Html::load($text);
    foreach ($this->getHeadings() as $heading) {
      $this->replaceHeadings($heading, $html_dom);
    }
    return new FilterProcessResult(Html::serialize($html_dom));
  }

  /**
   * @param $heading
   * @param \DOMDocument $document
   */
  protected function replaceHeadings($heading, \DOMDocument $document) {
    $headings = $document->getElementsByTagName($heading);
    foreach ($headings as $heading) {
      $heading->setAttribute('id', Html::getUniqueId($heading->textContent));
    }
  }

}
